# Copyright (c) 2016-present Sonatype, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM fernau/publishtest:0.0.2

ENV SONATYPE_DIR=/opt/sonatype \
    REPOSITORY='maven-releases' \
    FORMAT='maven2'

CMD ["sh", "-c", "groovy ${SONATYPE_DIR}/bin/NexusPublisher.groovy --username ${USERNAME} --password ${PASSWORD} --serverurl=${SERVER_URL} --filename=${FILENAME} --format=${FORMAT} --repository=${REPOSITORY} ${ATTRIBUTES}"]
