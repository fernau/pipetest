#!/usr/bin/env bash
set -ex

VERSION=$(semversioner current-version)

echo ${DOCKERHUB_PASSWORD} | docker login --username "$DOCKERHUB_USERNAME" --password-stdin

# get tags for releases or development branches
if [[ "${BITBUCKET_BRANCH}" == "master" ]]; then
    tag="${VERSION}"
elif [[ "${BITBUCKET_BRANCH}" =~ "INT-" ]]; then
    tag="${VERSION}-${BITBUCKET_BRANCH}"
else
    echo "ERROR: Branch name must match master or INT-*. Skipping push to docker."
    exit 0
fi

docker build -t ${DOCKERHUB_REPOSITORY}:pipelines-${BITBUCKET_BUILD_NUMBER} .

docker tag "${DOCKERHUB_REPOSITORY}:pipelines-${BITBUCKET_BUILD_NUMBER}" "${DOCKERHUB_REPOSITORY}:${tag}"
docker push "${DOCKERHUB_REPOSITORY}:${tag}"

# create :latest tag for builds on master
if [[ "${BITBUCKET_BRANCH}" == "master" ]]; then
  docker tag "${DOCKERHUB_REPOSITORY}:pipelines-${BITBUCKET_BUILD_NUMBER}" "${DOCKERHUB_REPOSITORY}:latest"
  docker push "${DOCKERHUB_REPOSITORY}:latest"
fi

# Update image attribute in pipe.yml
sed -i "s%^image.*$%image: ${DOCKERHUB_REPOSITORY}:${tag}%g" pipe.yml
