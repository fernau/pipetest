#!/usr/bin/env bash

set -e

cp /opt/atlassian/pipelines/agent/ssh/id_rsa ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa

git config user.name "CI user"
git config user.email noreply@sonatype.org
git remote set-url origin ${BITBUCKET_GIT_SSH_ORIGIN}
