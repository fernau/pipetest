#!/bin/bash

set -e

git config user.name "CI user"
git config user.email noreply@sonatype.org
git remote set-url origin ${BITBUCKET_GIT_SSH_ORIGIN}

if [[ "${BITBUCKET_BRANCH}" == "master" ]]; then

  # Tag and push
  tag=$(semversioner current-version)

  git add .
  git commit -m "Update files for new version '${tag}' [skip ci]"
  git push origin ${BITBUCKET_BRANCH}

  git tag -a -m "Tagging for release ${tag}" "${tag}"
  git push origin ${tag}

elif [[ "${BITBUCKET_BRANCH}" =~ "INT-" ]]; then
  if [[ `git status --porcelain` ]]; then
    git add .
    git commit -m "Updating files in development branch '${BITBUCKET_BRANCH}' [skip ci]"
    git push origin ${BITBUCKET_BRANCH}
  fi
fi
