# Bitbucket Pipelines Pipe: nexus-platform-pipes

This Bitbucket allows you to publish your artifacts to Nexus Repository Manager.
The latest version is published as: `sonatype/nexus-platform-pipes:0.1.5`

## YAML Definition
Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
    - step:
        # set NEXUS_USERNAME and NEXUS_PASSWORD as environment variables
        name: Deploy to Nexus Repository Manager
        deployment: test   # set to test, staging or production
        # trigger: manual  # uncomment to have a manual step
        script:
          - pipe: fernau/pipetest:0.1.5
            variables:
              FILENAME: "target/mini-1.0-SNAPSHOT.jar"
              ATTRIBUTES: "-CgroupId=com.example -CartifactId=myapp -Cversion=1.3 -Aextension=jar"
              USERNAME: "$NEXUS_USERNAME"
              PASSWORD: "$NEXUS_PASSWORD"
              SERVER_URL: "https://nexus.example.com/"
```

## Variables

| Variable                 | Usage | Example |
| --------------------------- | ----- | ----- |
| FILENAME (*)       | Path to file to publish. | target/demo-java-spring-0.0.1-SNAPSHOT.jar |
| ATTRIBUTES (*)     | Artifact attributes. | -CgroupId=com.example -CartifactId=myapp -Cversion=1.3 -Aextension=jar |
| USERNAME (*)       | Nexus username | admin |
| PASSWORD (*)       | Nexus password | admin123 |
| SERVER_URL (*)     | Nexus server URL | http://nexus.example.com/ |
| REPOSITORY         | Nexus repository name. Default: maven-releases |
| FORMAT             | Artifact format. Default: maven2 |
          
_(*) = required variable._

## Details
<pipe_long_description>

## Prerequisites
<pipe_prerequisites>

## Examples
<pipe_code_examples>

## Support
<pipe_support>
