# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.3

- patch: Merged in INT-test-branch (pull request #1)

INT test branch

## 0.1.2

- patch: push dev branches

## 0.1.1

- patch: update upstream docker image

## 0.1.0

- minor: updated gitignore
- patch: updated gitignore git message

## 0.0.6

- patch: add example snippet

## 0.0.5

- patch: fix args

## 0.0.4

- patch: pass parameters to script

## 0.0.3

- patch: fix table layout

## 0.0.2

- patch: short description and variables

## 0.0.1

- patch: `git log -1 --pretty=%B`
- patch: fix semversioner

